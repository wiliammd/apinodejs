const mysql = require('../mysql');

exports.getProdutos = async(req,res,next) =>{
    try {
        const result = await mysql.execute("Select *from produtos;").then((result)=>{
            const response = {
                       quantidade: result.length,
                       produtos: result.map(prod=>{
                           return {
                               id_produto: prod.id_produto,
                               nome: prod.nome,
                               preco: prod.preco,
                               imagem_produto:prod.imagem_produto,
                               request: {
                                   tipo:'GET',
                                   descricao:'Retorna os detalhes de um produto especifico',
                                   url:process.env.URL_API+'/'+'produtos/'+ prod.id_produto
                               }
                           }
                       })
                   }
                   return res.status(200).send(response);
        });       
    } catch (error) {
        return res.status(500).send({error:error});
    }

};


// exports.getProdutos = (req,res,next)=>{
//     mysql.getConnection((error,conn) => {
//         if(error){return res.status(500).send({error:error})}
//         conn.query(
//             'Select *from produtos;',
//             (error,result, fields) => {
//                 conn.release();
//                 if(error){return res.status(500).send({error:error})}
//                 const response = {
//                     quantidade: result.length,
//                     produtos: result.map(prod=>{
//                         return {
//                             id_produto: prod.id_produto,
//                             nome: prod.nome,
//                             preco: prod.preco,
//                             imagem_produto:prod.imagem_produto,
//                             request: {
//                                 tipo:'GET',
//                                 descricao:'Retorna os detalhes de um produto especifico',
//                                 url:process.env.URL_API+'/'+'produtos/'+ prod.id_produto
//                             }
//                         }
//                     })
//                 }
//                 return res.status(200).send({response})
//             }
//         )
//     });
// }
exports.postProdutos = (req,res,next)=>{
    console.log(req.usario)
   
    mysql.getConnection((error,conn) => {
        if(error){return res.status(500).send({error:error})}
        conn.query(
            'INSERT INTO produtos(nome,preco,imagem_produto) VALUES(?,?,?)',
            [req.body.nome, req.body.preco,req.file.path],
            (error, resultado, field) => {
                conn.release();
                if(error){return res.status(500).send({error:error})}
                const response = {
                    mensagem: 'Produto criado com sucesso!',
                    produtoCriado:{
                        id_produto:resultado.id_produto,
                        nome: req.body.nome,
                        preco:req.body.preco,
                        imagem_produto:req.file.path,
                        resquest:{
                                tipo:'POST',
                                descricao:'Retorna todos os produtos',
                                url:process.env.URL_API+'/produtos/'
                        }
                    }
                }
                return res.status(201).send({response});
            }
        )
    });
}
exports.getUmProduto = (req,res,next)=>{
    mysql.getConnection((error,conn) => {
        if(error){return res.status(500).send({error:error})}
        conn.query(
            'Select *from produtos WHERE id_produto = ?;',
            [req.params.id_produto],
            (error,result, fields) => {
                if(error){return res.status(500).send({error:error})}

                if(result.length == 0){
                    return res.status(404).send({
                        mensagem:'Não foi encontrado o ID'
                    });
                }
                const response = {
                
                    produto:{
                        id_produto:result[0].id_produto,
                        nome: result[0].nome,
                        preco: result[0].preco,
                        imagem_produto:result[0].imagem_produto,
                        resquest:{
                                tipo:'GET',
                                descricao:'Retorna um produto',
                                url:process.env.URL_API+'/produtos/'
                        }
                    }
                
            }
                return res.status(200).send(response);
            }
        )
    });
}
exports.pathProdutos = (req,res,next)=>{
    mysql.getConnection((error,conn) => {
        if(error){return res.status(500).send({error:error})}
        conn.query(
            `update produtos
                set nome =?,
                    preco=?
                where id_produto=?`,
            [req.body.nome, req.body.preco,req.body.id_produto],
            (error, resultado, field) => {
                conn.release();
                if(error){return res.status(500).send({error:error})}
                const response = {
                    mensagem: 'Produto atualizado com sucesso!',
                    produtoAtualizado:{
                        id_produto:resultado.id_produto,
                        nome: req.body.nome,
                        preco:req.body.preco,
                        resquest:{
                                tipo:'GET',
                                descricao:'Retorna todos os produtos',
                                url:process.env.URL_API+'/produtos/'+ req.body.id_produto
                        }
                    }
                }
                return res.status(202).send({response});
                res.status(202).send({
                    mensagem:'Produto alterado com sucesso'
                });
            }
        )
    });
}
exports.deleteProdutos = (req,res,next)=>{
    mysql.getConnection((error,conn) => {
        if(error){return res.status(500).send({error:error})}
        conn.query(
            'Delete from produtos where id_produto = ?',
            [req.body.id_produto],
            (error, result, field) => {
                conn.release();
                if(error){return res.status(500).send({error:error})}
                const response = {
                    mensagem:'Produto removido com sucesso',
                    request:{
                        tipo:'POST',
                        descricao:'Insere um produto',
                        url:process.env.URL_API+'/produtos',
                        body:{
                            nome:'String',
                            preco:'number'
                        }
                    }
                }
                res.status(202).send({response});
            }
        )
    });
}